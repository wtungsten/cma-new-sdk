function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function createFavouritesList() {
        favouritesList = [];
        favScrollView.removeAllChildren();
        Ti.API.info("creating favourites list");
        var favourites = dataBase.execute("SELECT * FROM favourites");
        while (favourites.isValidRow()) {
            var mid = favourites.fieldByName("id");
            var name = favourites.fieldByName("name");
            var lat = favourites.fieldByName("lat");
            var lon = favourites.fieldByName("lon");
            Ti.API.info("adding item : " + name);
            favouritesList.push({
                mid: mid,
                title: name,
                lat: lat,
                lon: lon
            });
            favourites.next();
        }
        favourites.close();
        0 == favouritesList.length && favouritesList.push({
            title: "You have no favourites"
        });
        Ti.API.info("comparing");
        favouritesList.sort(compare);
        Ti.API.info("list TO rows");
        listToRows(favouritesList);
        favourites = Ti.UI.createTableView({
            minRowHeight: 50,
            data: listToRow,
            separatorColor: "#FF9E1B"
        });
        favScrollView.add(favourites);
        favourites.addEventListener("click", function(e) {
            var mid = favouritesList[e.index]["mid"];
            var lat = favouritesList[e.index]["lat"];
            var lon = favouritesList[e.index]["lon"];
            map.region = {
                latitude: lat,
                longitude: lon,
                latitudeDelta: .0075,
                longitudeDelta: .0075
            };
            $.annotation.retrieveLocationDetails(mid);
            $.annotation.openAnnotationWindow();
        });
    }
    function compare(a, b) {
        if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
        if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
        return 0;
    }
    function listToRows(list) {
        listToRow = [];
        Ti.API.info("creating favourites list to rows");
        for (var d in list) {
            var name = list[d]["title"];
            var row = Ti.UI.createTableViewRow({
                height: 50,
                className: "tableRow"
            });
            var label = Ti.UI.createLabel({
                text: name,
                color: "black",
                font: {
                    fontsize: 16
                },
                textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
                left: 10,
                height: 50
            });
            Ti.API.info("row add :" + name);
            row.add(label);
            listToRow.push(row);
        }
        Ti.API.info("rows made");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "favourites";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.favourites = Ti.UI.createView({
        id: "favourites"
    });
    $.__views.favourites && $.addTopLevelView($.__views.favourites);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.annotation = Alloy.createController("annotation");
    $.openFavourites = function() {
        favouritesWindow.open();
        createFavouritesList();
    };
    $.updateList = function() {
        createFavouritesList();
    };
    favouritesWindow = Ti.UI.createWindow({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white",
        title: "Favourites"
    });
    var favouritesView = Ti.UI.createView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white"
    });
    var favScrollView = Ti.UI.createScrollView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white"
    });
    favouritesView.add(favScrollView);
    favouritesWindow.add(favouritesView);
    favouritesWindow.addEventListener("open", function() {
        var actionBar = favouritesWindow.activity.actionBar;
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.displayHomeAsUp = true;
        actionBar.onHomeIconItemSelected = function() {
            favouritesWindow.close();
        };
    });
    var favouritesList = [];
    Ti.UI.createTableView({});
    var listToRow = [];
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;