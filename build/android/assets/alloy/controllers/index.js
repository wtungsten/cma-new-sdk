function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function openURL() {
        Ti.API.info("opening");
        handleURL(Alloy.Globals.url);
    }
    function handleURL(url) {
        Ti.API.info("handling");
        var URL = $.XCallbackURL.parse(url), id = (URL.action(), URL.params(), URL.params("id"));
        Ti.API.info("id: " + JSON.stringify(id));
        var mid = id["id"];
        Ti.API.info("MID: " + mid);
        if (null != mid) {
            $.annotation.retrieveLocationDetails(mid);
            $.annotation.openAnnotationWindow();
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.categories = Alloy.createController("categories");
    $.search = Alloy.createController("search");
    $.annotation = Alloy.createController("annotation");
    $.menu = Alloy.createController("menu");
    $.routing = Alloy.createController("routing");
    $.XCallbackURL = Alloy.createController("XCallbackURL");
    Ti.UI.setBackgroundColor("#000");
    require("alloy/animation");
    var searchbtn;
    win.addEventListener("open", function() {
        var actionBar = win.activity.actionBar;
        actionBar.setIcon("/images/menu.png");
        actionBar.onHomeIconItemSelected = function() {
            $.menu.closeDialog();
            $.menu.addMenuWindow();
            $.menu.animateMenu();
        };
        openURL();
    });
    win.activity.onCreateOptionsMenu = function(e) {
        var menu = e.menu;
        clrbtn = menu.add({
            title: "Clear",
            icon: "images/clear.png",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS,
            visible: false
        });
        searchbtn = menu.add({
            title: "Search",
            icon: "images/search.png",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        clrbtn.addEventListener("click", function() {
            $.routing.clearRoute();
            clrbtn.setVisible(false);
            Ti.API.info("cleared route");
        });
        searchbtn.addEventListener("click", function() {
            $.menu.closeDialog();
            if ($.menu.getMenuOpen()) {
                $.menu.animateMenu();
                Ti.API.info("here2");
            }
            $.search.openSearch();
        });
    };
    var region = {
        latitude: -37.7207471,
        longitude: 145.04839,
        latitudeDelta: .01,
        longitudeDelta: .01
    };
    map = gmaps.createView({
        enableZoomControls: false,
        userLocationButton: false,
        traffic: true,
        animate: true,
        height: Ti.UI.FILL,
        mapType: gmaps.NORMAL_TYPE,
        region: region,
        regionFit: true,
        userLocation: true,
        width: Ti.UI.FILL
    });
    win.add(map);
    map.addEventListener("pinchangedragstate", function(e) {
        $.menu.closeMenu();
        sliding = false;
        toggle = false;
        win.left = 0;
        Ti.API.info(e.type);
        Ti.API.info(JSON.stringify(e.newState));
    });
    map.addEventListener("click", function(e) {
        $.menu.closeMenu();
        sliding = false;
        toggle = false;
        win.left = 0;
        Ti.API.info(e.type);
        Ti.API.info("Annotation ID: " + e.annotation.myid);
        Ti.API.info("Annotation Title: " + e.annotation.title);
        Ti.API.info(JSON.stringify(e.clicksource));
    });
    map.addEventListener("click", function(e) {
        $.annotation.retrieveLocationDetails(e.annotation.mid);
        if ("title" == e.clicksource || "infoWindow" == e.clicksource) {
            Ti.API.info("selected annotation title");
            selectedLocation = e.annotation.title;
            $.annotation.openAnnotationWindow();
        }
    });
    map.addEventListener("regionchanged", function(e) {
        $.menu.closeDialog();
        $.menu.getMenuOpen() && $.menu.animateMenu();
        Ti.API.info(e.type);
        Ti.API.info(e.latitude + "," + e.longitude);
    });
    map.addEventListener("complete", function(e) {
        Ti.API.info(e.type);
    });
    win.addEventListener("android:back", function() {
        if ($.menu.getMenuOpen()) $.menu.animateMenu(); else {
            win.close();
            var activity = Titanium.Android.currentActivity;
            dataBase.close();
            activity.finish();
        }
    });
    Ti.Gesture.addEventListener("orientationchange", function() {
        Ti.API.info("rotated");
        $.menu.removeWindow();
    });
    win.open();
    retrieveAllLocations();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;