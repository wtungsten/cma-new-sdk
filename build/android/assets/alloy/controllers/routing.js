function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function addRoute(obj) {
        Ti.API.info("in add route");
        var xhr = Ti.Network.createHTTPClient();
        xhr.onload = function() {
            Ti.API.info("Load");
            Ti.API.info("1");
            var response = this.responseText;
            var json = JSON.parse(response);
            var step = json.routes[0].legs[0].steps;
            var distance = json.routes[0].legs[0].distance.text;
            var duration = json.routes[0].legs[0].duration.text;
            var intStep = 0, intSteps = step.length, points = [];
            var decodedPolyline, intPoint = 0, intPoints = 0;
            for (intStep = 0; intSteps > intStep; intStep += 1) {
                decodedPolyline = decodeLine(step[intStep].polyline.points);
                intPoints = decodedPolyline.length;
                Ti.API.info("2");
                for (intPoint = 0; intPoints > intPoint; intPoint += 1) {
                    Ti.API.info("3");
                    if (null != decodedPolyline[intPoint]) {
                        Ti.API.info("4");
                        points.push({
                            latitude: decodedPolyline[intPoint][0],
                            longitude: decodedPolyline[intPoint][1]
                        });
                    }
                }
            }
            Ti.API.info("Distance: " + distance);
            Ti.API.info("Duration: " + duration);
            clearroute();
            addclear();
            route = gmaps.createRoute({
                name: "Example Route",
                points: points,
                color: "#c60000",
                width: 4
            });
            obj.map.addRoute(route);
            Ti.API.info("add route");
        };
        xhr.onerror = function(e) {
            Ti.API.info("error", JSON.stringify(e));
        };
        var param = [ "destination=" + obj.stop, "origin=" + obj.start, "sensor=true", "mode=walking" ];
        Ti.API.info(param.join("&"));
        obj.region && (param.region = obj.region);
        xhr.open("GET", "http://maps.googleapis.com/maps/api/directions/json?" + param.join("&"));
        xhr.send();
    }
    function decodeLine(encoded) {
        var len = encoded.length;
        var index = 0;
        var array = [];
        var lat = 0;
        var lng = 0;
        Ti.API.info("decoding");
        while (len > index) {
            var b;
            var shift = 0;
            var result = 0;
            do {
                b = encoded.charCodeAt(index++) - 63;
                result |= (31 & b) << shift;
                shift += 5;
            } while (b >= 32);
            var dlat = 1 & result ? ~(result >> 1) : result >> 1;
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charCodeAt(index++) - 63;
                result |= (31 & b) << shift;
                shift += 5;
            } while (b >= 32);
            var dlng = 1 & result ? ~(result >> 1) : result >> 1;
            lng += dlng;
            array.push([ 1e-5 * lat, 1e-5 * lng ]);
        }
        return array;
    }
    function newRoute() {
        if ("" != originName) {
            var originLocations = dataBase.execute("SELECT * FROM locations WHERE marker_name=?", originName);
            var found = false;
            while (originLocations.isValidRow() && !found) {
                var mname = originLocations.fieldByName("marker_name");
                var lat = originLocations.fieldByName("latitude");
                var lon = originLocations.fieldByName("longitude");
                if (originName.toLowerCase() == mname.toLowerCase()) {
                    orig = lat + "," + lon;
                    Ti.API.info("Origin: " + orig);
                    found = true;
                }
                originLocations.next();
            }
            originLocations.close();
        } else Ti.Geolocation.getCurrentPosition(function(e) {
            if (!e.success) {
                alert("Could not retrieve location");
                Ti.API.info("check");
                return;
            }
            latitude = e.coords.latitude;
            longitude = e.coords.longitude;
            orig = latitude + "," + longitude;
            Ti.API.info("user origin: " + orig);
            Ti.API.info(orig);
            latitude && null != longitude && (userLocation = gmaps.createAnnotation({
                latitude: latitude,
                longitude: longitude,
                image: "/images/pin.png",
                myid: 0,
                rightButton: Ti.UI.createButton({
                    title: "Details"
                }),
                leftButton: "",
                title: "My Location"
            }));
            originName = "your location";
        });
        if ("" != destinationName) {
            var destinationLocations = dataBase.execute("SELECT * FROM locations WHERE marker_name=?", destinationName);
            var found = false;
            while (destinationLocations.isValidRow() && !found) {
                var mname = destinationLocations.fieldByName("marker_name");
                var lat = destinationLocations.fieldByName("latitude");
                var lon = destinationLocations.fieldByName("longitude");
                if (destinationName.toLowerCase() == mname.toLowerCase()) {
                    dest = lat + "," + lon;
                    Ti.API.info("Destination: " + dest);
                    found = true;
                }
                destinationLocations.next();
            }
            destinationLocations.close();
        }
        Ti.API.info("destination value: " + destinationName);
        if ("" != dest) {
            addRoute({
                map: map,
                region: "AU",
                start: orig,
                stop: dest
            });
            var toast = Ti.UI.createNotification({
                message: "Routing from " + originName + " to " + destinationName,
                duration: Ti.UI.NOTIFICATION_DURATION_LONG
            });
            toast.show();
            Ti.API.info("addOrigin: " + orig);
            Ti.API.info("addDestination: " + dest);
        }
    }
    function clearroute() {
        if (null != route) {
            Ti.API.info("route is not null");
            map.removeRoute(route);
            originName = "";
            destinationName = "";
            orig = "";
            dest = "";
        }
    }
    function addclear() {
        Ti.API.info("added clear");
        clrbtn.setVisible(true);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "routing";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.routing = Ti.UI.createView({
        id: "routing"
    });
    $.__views.routing && $.addTopLevelView($.__views.routing);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.newRoute = function() {
        newRoute();
    };
    $.clearRoute = function() {
        clearroute();
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;