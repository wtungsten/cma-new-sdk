function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function animateMenu() {
        if (menuOpen) {
            Ti.API.info("closed menu");
            toggleMenu = true;
            menuWindow.animate(slideLeft);
            menuOpen = false;
        } else {
            Ti.API.info("open menu");
            toggleMenu = true;
            menuWindow.animate(slideRight);
            menuOpen = true;
        }
    }
    function menuItems() {
        menuTitles = [];
        for (var m in menuList) {
            var text = menuList[m];
            var image = menuImages[m];
            var rowView = Ti.UI.createTableViewRow({
                height: 50,
                className: "menuList"
            });
            var menuLabel = Ti.UI.createLabel({
                text: text,
                color: "white",
                font: {
                    fontsize: 6
                },
                textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
                left: 10
            });
            var menuImage = Ti.UI.createImageView({
                width: Ti.UI.SIZE,
                image: "/images/" + image + ".png"
            });
            var menuView = Ti.UI.createView({
                width: Ti.UI.FILL,
                layout: "horizontal"
            });
            menuView.add(menuImage);
            menuView.add(menuLabel);
            rowView.add(menuView);
            menuTitles.push(rowView);
            Ti.API.info("MENU: " + text + " & " + image);
        }
        tableView.data = menuTitles;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menu";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.menu = Ti.UI.createView({
        id: "menu"
    });
    $.__views.menu && $.addTopLevelView($.__views.menu);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.categories = Alloy.createController("categories");
    $.favourites = Alloy.createController("favourites");
    var menuOpen = false;
    var slideRight = Ti.UI.createAnimation({
        left: "0%",
        duration: 150
    });
    var slideLeft = Ti.UI.createAnimation({
        left: "-50%",
        duration: 150
    });
    $.addMenuWindow = function() {
        win.add(menuWindow);
        Ti.API.info("added menu");
        menuItems();
    };
    $.removeWindow = function() {
        menuOpen || win.remove(menuWindow);
    };
    $.closeMenu = function() {
        menuWindow.animate(slideLeft);
        menuOpen = false;
    };
    $.animateMenu = function() {
        animateMenu();
    };
    $.getMenuOpen = function() {
        return menuOpen;
    };
    $.closeDialog = function() {
        customDialog.hide();
    };
    var customDialog = Ti.UI.createView({
        height: 400,
        width: 300,
        backgroundColor: "#2D251A",
        borderRadius: 5
    });
    var titleLabel = Ti.UI.createLabel({
        top: 10,
        text: "Campus Maps Applicaton (CMA) v?.?? \n \n    Changes \n \n    - Fixed bug with search button\n    - Fixed several bugs with UI elements not \n      appearing on a single click \n\n    - Fixed overlapping of certain UI elements \n      now only one element is displyed at a time \n    - Fixed notification bar dissapearing when \n      app is loading then reappearing when the\n      map is displayed \n\n    - Annotation page is now themed\n    - Action bar is now themed\n    - UI elements are now themed\n    - Options in menu have icons\n    - La Trobe logos have replaced Tungsten ones \n\n    - Revamped the about page to conform with UI\n    - Menu button is now the app logo\n    - Search and clear buttons are now integrated\n      within action bar",
        color: "#FFF",
        font: {
            fontSize: 12,
            fontWeight: "bold"
        }
    });
    customDialog.add(titleLabel);
    var cancelBtn = Ti.UI.createButton({
        bottom: 5,
        left: 10,
        width: 280,
        height: 40,
        title: "Close",
        color: "#FFF"
    });
    customDialog.add(cancelBtn);
    menuWindow = Ti.UI.createView({
        top: 0,
        left: "-50%",
        width: "50%",
        backgroundColor: "#63513D",
        visible: true
    });
    var menuList = [ "Categories", "Favourites", "Update DB", "About" ];
    var menuImages = [ "categories", "favorites", "retrieve", "about" ];
    var menuTitles = [];
    var tableView = Ti.UI.createTableView({
        data: menuTitles,
        minRowHeight: 50
    });
    menuWindow.add(tableView);
    var toggleMenu = false;
    tableView.addEventListener("click", function(e) {
        e.index;
        if (0 == e.index) {
            Ti.API.info("Categories");
            $.categories.openCategories();
        } else if (1 == e.index) {
            Ti.API.info("Favourites");
            $.favourites.openFavourites();
        } else if (2 == e.index) retrieveAllLocations(); else if (3 == e.index) {
            Ti.API.info("About");
            sliding = false;
            toggleMenu = false;
            win.add(customDialog);
            customDialog.show();
            cancelBtn.addEventListener("click", function() {
                customDialog.hide();
            });
            animateMenu();
        } else {
            Ti.API.info("No option selected");
            alert("No option selected");
        }
    });
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;