var map;// instantiation of map object
var gmaps = require('ti.map');// this makes gmaps use the ti.map module
// checks the devices for Google Services, it will alert the user if there is an error
var rc = gmaps.isGooglePlayServicesAvailable();
switch (rc) {
    case gmaps.SUCCESS:
        Ti.API.info('Google Play services is installed.');
        break;
    case gmaps.SERVICE_MISSING:
        alert('Google Play services is missing. Please install Google Play services from the Google Play store.');
        break;
    case gmaps.SERVICE_VERSION_UPDATE_REQUIRED:
        alert('Google Play services is out of date. Please update Google Play services.');
        break;
    case gmaps.SERVICE_DISABLED:
        alert('Google Play services is disabled. Please enable Google Play services.');
        break;
    case gmaps.SERVICE_INVALID:
        alert('Google Play services cannot be authenticated. Reinstall Google Play services.');
        break;
    default:
        alert('Unknown error.');
        break;
}

// This will check for an input from a URL and save it in a global variable
if (Titanium.Platform.osname == 'android') {
    Ti.API.info('testing app open');
    // Somehow, only in alloy.js we can get the data (URL) that opened the app
    Alloy.Globals.url = Ti.Android.currentActivity.intent.data;
}

var locationArray = []; // this is not being used anymore since we added the database
var route = null; // the routes will be stored in here
var userLocation; // the user location will be stored in here

// database and table creation
var dataBase = Ti.Database.open('database');
dataBase.execute('CREATE TABLE IF NOT EXISTS locations(marker_id INTEGER, marker_name TEXT, type_id INTEGER, type_name TEXT, latitude REAL, longitude REAL);');
dataBase.execute('CREATE TABLE IF NOT EXISTS favourites(id INTEGER, name TEXT, lat REAL, lon REAL);');
dataBase.execute('CREATE TABLE IF NOT EXISTS serverMarkers(id INTEGER)');
dataBase.execute('DELETE FROM serverMarkers'); // this always clears the table for temporary storage on load
//dataBase.close();

// these variables store the device DPI and resolution for use
var deviceDPI = Titanium.Platform.displayCaps.dpi / 160;
var deviceHeight = Ti.Platform.displayCaps.platformHeight;
var deviceWidth = Ti.Platform.displayCaps.platformWidth;

// window creation start
var win = Ti.UI.createWindow({ // main window
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'black'
});
var clrbtn;

var searchWindow;

var categoriesWindow;

var favouritesWindow;

var menuWindow;
// window creationg end

// variables for origins and destinations
var orig = '';
var originName = '';
var dest = '';
var destinationName = '';

var skipped = 0;
var notSkipped = 0;
var removed = 0;

// initial build of the annotations from the database
function dataBaseBuild() {
	Ti.API.info('Building database');
	//retrieveAllLocations();
	var allLocations = dataBase.execute('SELECT * FROM locations');
	while(allLocations.isValidRow()) {
		var mid = allLocations.fieldByName('marker_id');
		var mname = allLocations.fieldByName('marker_name');
		var tid = allLocations.fieldByName('type_id');
		var tname = allLocations.fieldByName('type_name');
		var lat = allLocations.fieldByName('latitude');
		var lon = allLocations.fieldByName('longitude');
		
		locationBuild(mid, mname, tid, tname, lat, lon);
		allLocations.next();
	}
	allLocations.close();

	Ti.API.info('Number skipped = ' + skipped);
	Ti.API.info('Number not skipped = ' + notSkipped);
	Ti.API.info('Number removed = ' + removed);
}

// temporary array storage
var serverLocations = [];
// main method to build the database from the server
// locations will be checked with the database before re-adding them
function retrieveAllLocations() {
	Ti.API.info('Retrieving Locations');
	var xhr = Ti.Network.createHTTPClient();
	//alert('retrieving');
	xhr.onload = function (e) {
		var response = JSON.parse(this.responseText);
		//Ti.API.info(response.result);
		var error = JSON.stringify(response.error);
		var msg = JSON.stringify(response.msg);
		//Ti.API.info(error);
		if(error == "true") {
			//alert('error: ' + msg);
		} else {
			locationArray = [];
			serverLocations = response.result;
			//alert(dataArray);
			for(var d in serverLocations) {
				//Ti.API.info(d);
				//Ti.API.info(JSON.stringify(dataArray[d]['Name']));
				var mid = serverLocations[d]['marker_id'];
				var mname = serverLocations[d]['marker_name'];
				var tid = serverLocations[d]['type_id'];
				var tname = serverLocations[d]['type_name'];
				var lat = serverLocations[d]['latitude'];
				var lon = serverLocations[d]['longitude'];
				
				dataBase.execute('INSERT INTO serverMarkers(id) VALUES (?)', mid);
				
				var newMarker = dataBase.execute('SELECT marker_id FROM locations WHERE marker_id=?', mid);
				var exists = false;
				while(newMarker.isValidRow() && !exists) {
					var tempMarker = newMarker.fieldByName('marker_id');
					if(mid == tempMarker) {
						Ti.API.info('Marker already exists. SKIPPING');
						exists = true;
						skipped = skipped+1;
					} else {
						Ti.API.info('New Marker');
						notSkipped = notSkipped+1;
					}
					newMarker.next();
				}
				if(!exists) {
					Ti.API.info('adding new marker');
					dataBase.execute('INSERT INTO locations(marker_id, marker_name, type_id, type_name, latitude, longitude) VALUES (?,?,?,?,?,?)', mid, mname, tid, tname, lat, lon);
					locationBuild(mid, mname, tid, tname, lat, lon);
				}
				newMarker.close();
				//dataBase.close();
				/*locationArray.push({marker_id: mid, name: mname,
					type_id: tid, type_name: tname, lat: lat, lon: lon});
				
				locationBuild(mid, mname, tid, tname, lat, lon);*/
				//Ti.API.info(JSON.parse(i));
				//Ti.API.info("In loop");
			}
			//dataBase.execute('INSERT INTO locations(marker_id, marker_name, type_id, type_name, latitude, longitude) VALUES (?,?,?,?,?,?)', 333, 'test', 333, 'test', 333, 333);
			
			checkForRemoved();
			//Ti.API.info("After loop");
		}
	};
	xhr.onerror = function (e) {
        alert('error: Could not connect to server. Check your connection.', JSON.stringify(e));
    };
	xhr.open('POST','http://www.wtungsten.com/app/functions.php');
	xhr.send({
		'action': 'parentMarkers'
	});
}

// checks the database with the temporary table for any locations that have been removed
function checkForRemoved() {
	Ti.API.info('Checking for removed locations');
	var change = false;
	var markers = dataBase.execute('SELECT marker_id FROM locations');
	while(markers.isValidRow() && !change) {
		var temp = markers.fieldByName('marker_id');
		var check = dataBase.execute('SELECT id FROM serverMarkers');
		var found = false;
		Ti.API.info('temp: ' + temp);
		while(check.isValidRow() && !found) {
			var match = check.fieldByName('id');
			Ti.API.info('checking: ' + temp + ' to ' + match);
			if (temp == match) {
				found = true;
				Ti.API.info('found');
			}
			check.next();
		}
		check.close();
		
		if(!found) {
			Ti.API.info('Deleting: ' + temp);
			dataBase.execute('DELETE FROM locations WHERE marker_id=?', temp);
			dataBase.execute('DELETE FROM favourites WHERE id=?', temp);
			removed = removed+1;
		}
		markers.next();
	}
	markers.close();
	dataBaseBuild();
}

// data is sent to here, then an annotation is created
function locationBuild(mid, mname, tid, tname, lat, lon) {
	Ti.API.info("Build");
	var annotation = gmaps.createAnnotation({
		latitude: lat,
		longitude: lon,
		image:'/images/pin.png',
		//myid:1, // Custom property to uniquely identify this annotation.
		//rightButton: Ti.UI.createButton({title: 'Details'}),
		//leftButton: '/images/agoraSmall.jpg',
		title: mname,
		mid: mid,
		tid: tid,
		tname: tname
	});
	map.addAnnotation(annotation);
	Ti.API.info("added");
}