//opens the search window
$.openSearch = function() {
	searchWindow.open();
};

$.annotation = Alloy.createController('annotation'); // connection to annotation controller

// new search windows
var searchOpen = false;

searchWindow = Ti.UI.createWindow({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white',
	title:'Search Locations'
});

searchWindow.layout = 'vertical';

// main search view
var searchView = Ti.UI.createView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white'
});

// unused button
var searchWindowBtn = Ti.UI.createButton({
	height: 39,
	width: 39,
	right: 0,
	borderWidth:1,
	opacity:0.7,
	backgroundTopCap:10,
	backgroundImage:'/images/search.png',
	backgroundColor:'#FFF',
	color: '#666',
	borderColor: '#666',
});

// search bar for search term
var searchBar = Titanium.UI.createTextField({
    barColor:'#000',
    color:'black',
    showCancel:false,
    height:50,
    width: Titanium.UI.FILL,
    top:0,
    opacity:1,
    hintText: 'Search',
    returnKeyType: Ti.UI.RETURNKEY_SEARCH
});

//searchView.add(searchBar);
//searchView.add(searchWindowBtn);

// view to contain the list of results from the server
var searchScrollView = Ti.UI.createScrollView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white'
});
	
//searchWindow.add(searchView);
searchWindow.add(searchBar);
searchWindow.add(searchScrollView);

// search bar within the search window
searchBar.addEventListener('return', function (e) {
	//alert('enter pressed');
	searchLocations(searchBar.value);
});

// serverSearchList is a temporary list to store the response
// from the server searchListItems is to store results from 
// serverSearchList searchList is the table to display of results
var serverSearchList = [];
var searchListItems = [];
var searchList = Ti.UI.createTableView({});

// sends a search term to the server and retrieves a list
function searchLocations(search) {
	var xhr = Ti.Network.createHTTPClient();
	xhr.onload = function (e) {
		var response = JSON.parse(this.responseText);
		Ti.API.info(response.result);
		var error = JSON.stringify(response.error);
		var msg = JSON.stringify(response.msg);
		msg = msg.replace(/"/g, "");
		Ti.API.info(error);
		if(error == "true") {
			//alert('error: ' + msg);
			searchListItems = [];
			if(msg == "No results")
			{
				searchListItems.push({title: msg, error: true});
			}
		} else {
			serverSearchList = response.result;
			//alert(JSON.stringify(dataArray));
			searchListItems = [];
			for(var d in serverSearchList) {
				Ti.API.info("In loop");
				var mid = serverSearchList[d]['marker_id'];
				var name = serverSearchList[d]['marker_name'];
				var tid = serverSearchList[d]['type_id'];
				var tname = serverSearchList[d]['type_name'];
				var lat = serverSearchList[d]['latitude'];
				var lon = serverSearchList[d]['longitude'];
				Ti.API.info(name);
				//addSearchListItems(name);
				
				searchListItems.push({mid: mid, title: name, 
					tid: tid, tname: tname, lat: lat, lon: lon});
			}
			Ti.API.info("After loop");
		}
		addSearchListItems();
	};
	xhr.onerror = function (e) {
        alert('error: ', JSON.stringify(e));
    };
	xhr.open('POST','http://www.wtungsten.com/app/functions.php');
	xhr.send({
		'action': 'searchMarkers',
		'search': search
	});
}

// add the results list to the table to show on the screen
function addSearchListItems() {
	Ti.API.info('adding lists');
	searchScrollView.removeAllChildren();
	searchList.data = [];
	searchListItems.sort(compare);
	listToRows(searchListItems);
	Ti.API.info("listToRow: " + JSON.stringify(listToRow));
	searchList = Ti.UI.createTableView({
		minRowHeight: 50,
		data: listToRow,
		separatorColor: '#FF9E1B'
	});
	//searchList.setData(listToRow);
	Ti.API.info("listToRow: " + JSON.stringify(searchList.data));
	searchScrollView.add(searchList);
	
	searchList.addEventListener('click', function (e) {
		// if the result is not an error, the selected result
		// will open an annotation for the location
		if(!searchListItems[e.index]['error']){
			var mid = searchListItems[e.index]['mid'];
			var item = searchListItems[e.index]['title'];
			var tid = searchListItems[e.index]['tid'];
			var tname = searchListItems[e.index]['tname'];
			var lat = searchListItems[e.index]['lat'];
			var lon = searchListItems[e.index]['lon'];
			Ti.API.info(item);
			//searchWindow.close();
			//locationBuild(mid, item, tid, tname, lat, lon);
			
			// this moves the map to the location
			map.region = ({latitude: lat, longitude: lon, 
				latitudeDelta: 0.0075, longitudeDelta: 0.0075});
			
			$.annotation.retrieveLocationDetails(mid);
			$.annotation.openAnnotationWindow();
		}
	});
}

searchWindow.addEventListener('open', function() {
	var actionBar = searchWindow.activity.actionBar;
	actionBar.setDisplayShowHomeEnabled(false);
	actionBar.displayHomeAsUp = true;
	actionBar.onHomeIconItemSelected = function() 
	{ 
		searchWindow.close();
	};
});

/*searchWindow.addEventListener("open", function(evt) { 
	var searchBar = searchWindow.activity.actionBar; 
	searchBar.displayHomeAsUp = true;
	searchBar.onHomeIconItemSelected = function() 
	{ 
		searchWindow.close();
	};
});*/

// compare function to sort lists
function compare(a,b) {
  if (a.title.toLowerCase() < b.title.toLowerCase())
     return -1;
  if (a.title.toLowerCase() > b.title.toLowerCase())
    return 1;
  return 0;
}

var listToRow = [];
function listToRows(list) {
	listToRow = [];
	for(var d in list) {
		var name = list[d]['title'];
	
		var row = Ti.UI.createTableViewRow({
			height: 50,
			className: "tableRow"
		});
		
		var label = Ti.UI.createLabel({
			text: name,
			color: 'black',
			font: { fontsize: 16 },
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
			left: 10,
			height: 50
		});
		
		Ti.API.info("row add :" + name);
		row.add(label);
		listToRow.push(row);
	}
}