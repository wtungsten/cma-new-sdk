// Route start

// new route method
// the addRoute and decodeLine methods contact the Google Maps API and retrieve a route from it.
function addRoute(obj) {
	Ti.API.info('in add route');
    var xhr = Ti.Network.createHTTPClient();
    xhr.onload = function (e) {
    	Ti.API.info('Load');
    	Ti.API.info('1');
        var response = this.responseText;
        var json = JSON.parse(response);
        var step = json.routes[0].legs[0].steps;
        var distance = json.routes[0].legs[0].distance.text;
        var duration = json.routes[0].legs[0].duration.text;
        var intStep = 0, intSteps = step.length, points = [];
        var decodedPolyline, intPoint = 0, intPoints = 0;
        for (intStep = 0; intStep < intSteps; intStep = intStep + 1) {
            decodedPolyline = decodeLine(step[intStep].polyline.points);
            intPoints = decodedPolyline.length;
            Ti.API.info('2');
            for (intPoint = 0; intPoint < intPoints; intPoint = intPoint + 1) {
                Ti.API.info('3');
                if (decodedPolyline[intPoint] != null) {
                    Ti.API.info('4');
                    points.push({
                        latitude: decodedPolyline[intPoint][0],
                        longitude: decodedPolyline[intPoint][1]
                    });
                }
            }
        }
 		
        /*route = {
            name: 'Example Route',
            points: points,
            color: '#c60000',
            width: 4
        };*/
        Ti.API.info('Distance: ' + distance);
        Ti.API.info('Duration: ' + duration);
        //alert('Distance: ' + distance + '\nDuration: ' + duration);
        clearroute();
    	addclear();
        
        route = gmaps.createRoute ({
            name: 'Example Route',
            points: points,
            color: '#c60000',
            width: 4
        });
        
        obj.map.addRoute(route); 
        Ti.API.info('add route');
    };
    xhr.onerror = function (e) {
        Ti.API.info('error', JSON.stringify(e));
    };
	var param = [
	    'destination=' + obj.stop,
	    'origin=' + obj.start,
	    'sensor=true' , 'mode=walking'
	  	];
	  	
    Ti.API.info(param.join('&'));
	if (obj.region) {
	    param.region = obj.region;
	}
    xhr.open('GET', 'http://maps.googleapis.com/maps/api/directions/json?' + param.join('&'));
    xhr.send();
} 

function decodeLine(encoded) {
    var len = encoded.length;
    var index = 0;
    var array = [];
    var lat = 0;
    var lng = 0;
 	Ti.API.info('decoding');
    while (index < len) {
        var b;
        var shift = 0;
        var result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
 
        var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
 
        shift = 0;
        result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
 
        var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
 
        array.push([lat * 1e-5, lng * 1e-5]);
    }
 
    return array;
}
// Route end

// forward to newRoute function
$.newRoute = function () {
	newRoute();
};

// this will check the database for the origin and destination, and will then get its coordinates and save them to the orig and dest variables
// orig is determined by either user location or where a user has selected
function newRoute() {
	//Ti.API.info('searchboxes are now: ' + searchboxes);
    //Origin calls
    /*if (origin.value == 'bg') {
    	orig = '-37.72142273370913,145.04683412611485',
    	Ti.API.info('orig is now; ' + orig),
    	Ti.API.info('origin now bg');
	};
	
	//Destination calls
    if (destination.value == 'wlt') {
    	dest = '-37.71950370947575,145.04558186978102',
    	Ti.API.info('dest is now;' + dest),
    	Ti.API.info('destination now wlt');
	} else if (destination.value == 'swa') {
    	dest = '-37.72185554159098,145.0469209626317',
    	Ti.API.info('dest is now; ' + dest),
    	Ti.API.info('destination now swa');
	}; */

	// new origin and destination methods
	if(originName != '') {
		var originLocations = dataBase.execute('SELECT * FROM locations WHERE marker_name=?', originName);
		var found = false;
		while(originLocations.isValidRow() && !found) {
			var mname = originLocations.fieldByName('marker_name');
			var lat = originLocations.fieldByName('latitude');
			var lon = originLocations.fieldByName('longitude');
			if(originName.toLowerCase() == mname.toLowerCase()) {
				orig = lat + ',' + lon;
				Ti.API.info('Origin: ' + orig);
				found = true;
			}
			originLocations.next();
		}
		originLocations.close();
	} else {
		// get current location
		Ti.Geolocation.getCurrentPosition( function(e) {
			if (!e.success) {
				alert('Could not retrieve location');
				Ti.API.info('check');
				return;
			}
			
			//here are users coordinates
			latitude = e.coords.latitude;
			longitude = e.coords.longitude;
			orig = latitude + ',' + longitude;  
			Ti.API.info('user origin: ' + orig);
			//origin.value = 'Your Location (or Type Origin)';
			Ti.API.info(orig);
			
			if (latitude && longitude != null) {
				userLocation = gmaps.createAnnotation({
					latitude: latitude,
					longitude: longitude,
					image:'/images/pin.png', 
					myid:0, 
					rightButton: Ti.UI.createButton({title: 'Details'}),
					leftButton: '',
					title: 'My Location',
				});
				//map.addAnnotation(userLocation);
			}
			
			originName = "your location";
		});
	}
	
	if(destinationName != '') {
		var destinationLocations = dataBase.execute('SELECT * FROM locations WHERE marker_name=?', destinationName);
		var found = false;
		while(destinationLocations.isValidRow() && !found) {
			var mname = destinationLocations.fieldByName('marker_name');
			var lat = destinationLocations.fieldByName('latitude');
			var lon = destinationLocations.fieldByName('longitude');
			if(destinationName.toLowerCase() == mname.toLowerCase()) {
				dest = lat + ',' + lon;
				Ti.API.info('Destination: ' + dest);
				found = true;
			}
			destinationLocations.next();
		}
		destinationLocations.close();
	}
    
    Ti.API.info("destination value: " + destinationName);
    if (dest != '') {
	    //blursearch();
	    
	    addRoute({
	        map: map,
	        region: 'AU',
	        start: orig,              
	        stop: dest  
	    });
	    
	    var toast = Ti.UI.createNotification({
			message: 'Routing from ' + originName + ' to ' + destinationName,
			duration: Ti.UI.NOTIFICATION_DURATION_LONG
		});
		toast.show();
	    Ti.API.info('addOrigin: ' + orig);
	    Ti.API.info('addDestination: ' + dest); 
    }
    
    //dest = '';
    
    //hidebuttons();
}

// forward to clearRoute function
$.clearRoute = function () {
	clearroute();
};

// clears the route from the map
function clearroute() {
	if(route != null) {
		Ti.API.info('route is not null');
		map.removeRoute(route);
		originName = '';
		destinationName = '';
		orig = '';
		dest = '';
	};
}

// sets the clear button to visible
function addclear() {
	Ti.API.info('added clear');
	clrbtn.setVisible(true);
}