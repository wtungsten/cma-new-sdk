$.annotation = Alloy.createController('annotation'); // connection to annotation controller

// opens the favourites window
$.openFavourites = function () {
	favouritesWindow.open();
	createFavouritesList();
};

// updates the list of favourites
$.updateList = function () {
	createFavouritesList();
};

// creation of the favourites window
favouritesWindow = Ti.UI.createWindow({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white',
	title:'Favourites'
});

// main view for the window
var favouritesView = Ti.UI.createView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white'
});

// scrollable view for the list
var favScrollView = Ti.UI.createScrollView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white'
});

favouritesView.add(favScrollView);
favouritesWindow.add(favouritesView);

favouritesWindow.addEventListener('open', function() {
	var actionBar = favouritesWindow.activity.actionBar;
	actionBar.setDisplayShowHomeEnabled(false);
	actionBar.displayHomeAsUp = true;
	actionBar.onHomeIconItemSelected = function() 
	{ 
		favouritesWindow.close();
	};
});

// creates a favourites list and adds it to the view
var favouritesList = [];
var favourites = Ti.UI.createTableView({});
function createFavouritesList() {
	favouritesList = [];
	favScrollView.removeAllChildren();
	Ti.API.info('creating favourites list');
	var favourites = dataBase.execute('SELECT * FROM favourites');
	while (favourites.isValidRow()) {
		var mid = favourites.fieldByName('id');
		var name = favourites.fieldByName('name');
		var lat = favourites.fieldByName('lat');
		var lon = favourites.fieldByName('lon');
		Ti.API.info('adding item : ' + name);
		favouritesList.push({mid: mid, title: name, lat: lat, lon: lon});
		favourites.next();
	}
	favourites.close();
	
	if(favouritesList.length == 0)
	{
		favouritesList.push({title: 'You have no favourites'});
	}
	
	Ti.API.info('comparing');
	favouritesList.sort(compare);
	Ti.API.info('list TO rows');
	listToRows(favouritesList);
	
	favourites = Ti.UI.createTableView({
		minRowHeight: 50,
		data: listToRow,
		separatorColor: '#FF9E1B'
	});
	favScrollView.add(favourites);
	
	favourites.addEventListener('click', function(e) {
		var mid = favouritesList[e.index]['mid'];
		var lat = favouritesList[e.index]['lat'];
		var lon = favouritesList[e.index]['lon'];
		// this moves the map to the location
		map.region = ({latitude: lat, longitude: lon, 
			latitudeDelta: 0.0075, longitudeDelta: 0.0075});
		
		$.annotation.retrieveLocationDetails(mid);
		$.annotation.openAnnotationWindow();
	});
};

function compare(a,b) {
  if (a.title.toLowerCase() < b.title.toLowerCase())
     return -1;
  if (a.title.toLowerCase() > b.title.toLowerCase())
    return 1;
  return 0;
}

var listToRow = [];
function listToRows(list) {
	listToRow = [];
	Ti.API.info('creating favourites list to rows');
	for(var d in list) {
		var name = list[d]['title'];
	
		var row = Ti.UI.createTableViewRow({
			height: 50,
			className: "tableRow"
		});
		
		var label = Ti.UI.createLabel({
			text: name,
			color: 'black',
			font: { fontsize: 16 },
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
			left: 10,
			height: 50
		});
		
		Ti.API.info("row add :" + name);
		row.add(label);
		listToRow.push(row);
	}
	Ti.API.info('rows made');
}
