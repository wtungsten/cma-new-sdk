// opens categories window
$.openCategories = function() {
	categoriesWindow.open();
	retrieveCategories();
};

$.annotation = Alloy.createController('annotation'); // connection to annotation controller

// retrieves list of categories from the server
categoriesWindow = Ti.UI.createWindow({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white',
	title:'Categories'
});
	
categoriesWindow.layout = 'vertical';

// view to contain the list of categories from the server 
var categoriesView = Ti.UI.createScrollView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: '#white'});

categoriesWindow.add(categoriesView);

var categoriesOpen = false;

// serverCategoryList is a temporary list to store the 
// response from the server categoriesListItems is to store
// results from serverSearchList categoriesList is the table
// to display of results
var serverCategoriesList = [];
var categoriesListItems = [];
var categoriesList = Ti.UI.createTableView({});

// retrieves a list of categories from the server
function retrieveCategories() {
	var xhr = Ti.Network.createHTTPClient();
	xhr.onload = function (e) {
		var response = JSON.parse(this.responseText);
		Ti.API.info(response.result);
		var error = JSON.stringify(response.error);
		var msg = JSON.stringify(response.msg);
		Ti.API.info(error);
		if(error == "true") {
			//alert('error: ' + msg);
			categoriesListItems = [];
			categoriesListItems.push({title: msg, error: true});
		} else {
			serverCategoriesList = response.result;
			//alert(JSON.stringify(dataArray));
			categoriesListItems = [];
			for(var d in serverCategoriesList) {
				var cid = serverCategoriesList[d]['id'];
				var cname = serverCategoriesList[d]['name'];
				
				categoriesListItems.push({cid: cid, title: cname});
			}
			Ti.API.info("After loop");
		}
		addCategoriesListItems();
	};
	xhr.onerror = function (e) {
        alert('error: ', JSON.stringify(e));
    };
	xhr.open('POST','http://www.wtungsten.com/app/functions.php');
	xhr.send({
		'action': 'categories'
	});
}

// add the results list to the table to show on the screen
function addCategoriesListItems() {
	Ti.API.info('adding lists');
	categoriesView.removeAllChildren();
	categoriesList.data = [];
	categoriesListItems.sort(compare);
	listToRows(categoriesListItems);
	categoriesList = Ti.UI.createTableView({
		minRowHeight: 50,
		data: listToRow,
		separatorColor: '#FF9E1B'
	});
	categoriesView.add(categoriesList);
	
	categoriesList.addEventListener('click', function (e) {
		// if the result is not an error, the selected result will 
		//send the id of the category to the retrieveLocationsOfCategory method
		if(!categoriesListItems[e.index]['error']) {
			var category = categoriesListItems[e.index]['cid'];
			selectedCategory = true;
			retrieveLocationsOfCategory(category);
			categoriesWindow.title = categoriesListItems[e.index]['title'];
		}
	});
}

// serverCategoryLocations is a temporary list to store 
// the response from the server categoryItems is to 
// store results from serverSearchList categoriesList is
// the table to display of results
var serverCategoryLocations = [];
var categoryLocations = [];
var selectedCategory = false;

function retrieveLocationsOfCategory(category) {
	var xhr = Ti.Network.createHTTPClient();
	//alert('retrieving');
	xhr.onload = function (e) {
		var response = JSON.parse(this.responseText);
		Ti.API.info(response.result);
		var error = JSON.stringify(response.error);
		var msg = JSON.stringify(response.msg);
		Ti.API.info(error);
		if(error == "true") {
			//alert('error: ' + msg);
			categoryLocations = [];
			categoryLocations.push({title: msg, error: true});
		} else {
			serverCategoryLocations = response.result;
			categoryLocations = [];
			//alert(dataArray);
			for(var d in serverCategoryLocations) {
				//Ti.API.info(d);
				//Ti.API.info(JSON.stringify(dataArray[d]['Name']));
				var mid = serverCategoryLocations[d]['marker_id'];
				var mname = serverCategoryLocations[d]['marker_name'];
				var lat = serverCategoryLocations[d]['latitude'];
				var lon = serverCategoryLocations[d]['longitude'];
				//Ti.API.info(JSON.parse(i));
				categoryLocations.push({mid: mid, title: mname, lat: lat, lon: lon});
				Ti.API.info("In loop");
			}
			Ti.API.info("After loop");
		}
		addMarkersOfCategoryItems();
	};
	xhr.onerror = function (e) {
        alert('error: ', JSON.stringify(e));
    };
	xhr.open('POST','http://www.wtungsten.com/app/functions.php');
	xhr.send({
		'action': 'markersOfType',
		'search': category
	});
}

// retrieves a list of categories from the server, this 
// adds the list to the same table as before (categoriesList)
function addMarkersOfCategoryItems() {
	Ti.API.info('adding lists');
	categoriesView.removeAllChildren();
	categoriesList.data = [];
	categoryLocations.sort(compare);
	listToRows(categoryLocations);
	categoriesList = Ti.UI.createTableView({
		minRowHeight: 50,
		data: listToRow,
		separatorColor: '#FF9E1B'
	});
	categoriesView.add(categoriesList);
	
	categoriesList.addEventListener('click', function (e) {
		//Ti.API.info(searchListButton.getTitle());
		//Ti.API.info('search button clicked');
		if(!categoryLocations[e.index]['error']) {
			var mid = categoryLocations[e.index]['mid'];
			var lat = categoryLocations[e.index]['lat'];
			var lon = categoryLocations[e.index]['lon'];
			Ti.API.info("Marker ID: " + mid);
			
			// this moves the map to the location
			map.region = ({latitude: lat, longitude: lon,
				latitudeDelta: 0.0075, longitudeDelta: 0.0075});
			
			$.annotation.retrieveLocationDetails(mid);
			$.annotation.openAnnotationWindow();
			//map.fireEvent('click');
		}
	});
}

// this will check if a category is selected. it will go back to 
//the categories list if a category has been selected or will close
//the window if one has
function closeCategories() {
	if (selectedCategory) {
		addCategoriesListItems();
		selectedCategory = false;
		Ti.API.info('selected category backpressed 2');
		categoriesWindow.title = 'Categories';
	} else {
		categoriesWindow.close();
		categoriesOpen = false;
		Ti.API.info('category backpressed 2');
	}
}
categoriesWindow.addEventListener('android:back',function(e) {
	closeCategories();
});

categoriesWindow.addEventListener('open', function() {
	var actionBar = categoriesWindow.activity.actionBar;
	actionBar.setDisplayShowHomeEnabled(false);
	actionBar.displayHomeAsUp = true;
	actionBar.onHomeIconItemSelected = function() 
	{ 
		closeCategories();
	};
});

function compare(a,b) {
  if (a.title.toLowerCase() < b.title.toLowerCase())
     return -1;
  if (a.title.toLowerCase() > b.title.toLowerCase())
    return 1;
  return 0;
}

var listToRow = [];
function listToRows(list) {
	listToRow = [];
	for(var d in list) {
		var name = list[d]['title'];
	
		var row = Ti.UI.createTableViewRow({
			height: 50,
			className: "tableRow"
		});
		
		var label = Ti.UI.createLabel({
			text: name,
			color: 'black',
			font: { fontsize: 16 },
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
			left: 10,
			height: 50
		});
		
		Ti.API.info("row add :" + name);
		row.add(label);
		listToRow.push(row);
	}
}