$.routing = Alloy.createController('routing');
//$.favourites = Alloy.createController('favourites');

// new anno window
var annoWindow = Ti.UI.createWindow({
	fullscreen:false,
	navBarHidden:false,
	backgroundColor: 'white',
	title:'Loading...',});
	
var annoOpen = false;
//annoWindow.layout = 'vertical';

var percentage = (1-(50/(deviceHeight/deviceDPI)))*100 + '%';

// this view will contain the annotation
// image, heading and description
var annoView = Ti.UI.createView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white'
});

annoView.layout = 'vertical';
// scroll view for the description
var annoScrollView = Ti.UI.createScrollView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white',
	height: percentage,
	top: 0
});												

/*var annoImage = Ti.UI.createImageView({
	top: 10,
	//height: '50'
});
annoView.add(annoImage);*/

// holds the imageCaptionView
var imageArea = Ti.UI.createView({
	height: Ti.UI.SIZE
});

// holds image and caption
var imageCaptionView = Ti.UI.createScrollableView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: '#EBE8E1',
	height: '50%',
	top: 0
});										
var views = []; // array of views for the images
imageCaptionView.layout = 'vertical';
annoView.add(imageArea);

// unused heading
var heading = Ti.UI.createLabel({
	color: 'black',
	text: 'Test',
	//textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
	font: { fontSize:24 },
	//height: 40,
	width: '95%',
	//top: 200,
	left: 10,
	//borderColor: 'white',
	//borderWidth: 1
});
//annoView.add(heading);

// holds the description and other information from the server
var descrip = Ti.UI.createLabel({
	color: 'black',
	text: 'TLoading...',
	//textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
	verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
	font: { fontSize: 16 },
	//height: 160,
	width: '95%',
	//top: 240,
	left: 10,
	//borderColor: 'white',
	//borderWidth: 1
});
annoView.add(descrip);

//annoWindow.add(annoScrollView);

// seperate view for the buttons
var buttonView = Ti.UI.createView({
	fullscreen:false,
	navBarHidden:true,
	backgroundColor: 'white',
	bottom: 0,
	height: Ti.UI.SIZE});

var routeToButton = Ti.UI.createButton({
	title: 'Route To',
	right: 0,
	bottom: 0,
	height: 50,
	width: '50%'
});

var routeFromButton = Ti.UI.createButton({
	title: 'Route From',
	left: 0,
	bottom: 0,
	height: 50,
	width: '50%'
});
buttonView.add(routeToButton);
buttonView.add(routeFromButton);
annoScrollView.add(annoView);
annoWindow.add(annoScrollView);
annoWindow.add(buttonView);

var selectedLocation = '';

var details = [];
var markerID;
var markerName;
var markerLat;
var markerLon;

// retrieves details for a location from the server
$.retrieveLocationDetails = function (mid) {
	checkFavourite(mid);
	annoWindow.setTitle('Loading...');
	descrip.setText('Loading...');
	var xhr = Ti.Network.createHTTPClient();
	Ti.API.info('retrieving');
	xhr.onload = function (e) {
		var response = JSON.parse(this.responseText);
		//Ti.API.info(response.result);
		var error = JSON.stringify(response.error);
		var msg = JSON.stringify(response.msg);
		//Ti.API.info(error);
		markerID = mid;
		if(error == "true") {
			//alert('error: aerr' + msg + 'mid:' + mid);
			addDetails({marker_name: 'Error', description:'Location could not be found on the server', inputs: ''});
		} else {
			details = response.result;
			markerName = details[0]['marker_name'];
			markerLat = details[0]['latitude'];
			markerLon = details[0]['longitude'];
			addDetails(details[0]);
		}
	};
	xhr.onerror = function (e) {
        alert('error: ', JSON.stringify(e));
    };
	xhr.open('POST','http://www.wtungsten.com/app/functions.php');
	xhr.send({
		'action': 'markerFromId',
		'search': mid
	});
};


// adds details to the annotation window
function addDetails(details) {
	var title = JSON.stringify(details['marker_name']);
	title = title.replace(/"/g, "");
	Ti.API.info('title: ' + title);
	annoWindow.setTitle(title);
	var description = JSON.stringify(details['description']);
	Ti.API.info(description);
	description = description.replace(/"/g, "");
	description = description.replace(/\\r/g, "");
	Ti.API.info(description);
	description = description.replace(/\\n/g, "\n");
	Ti.API.info(description);
	var inputs = JSON.stringify(details['inputs']);
	Ti.API.info(inputs);
	inputs = inputs.replace(/[{}\[\]\\\"]/g,"");
	inputs = inputs.replace(/:/g, ": ");
	inputs = inputs.replace(/,/g, "\n");
	Ti.API.info(inputs);
	description = description + "\n\n" + inputs;
	heading.setText(title);
	descrip.setText(description);
	
	var imageArray = details['images'];
	imageArea.removeAllChildren();
	views = [];
	for(var i in imageArray) {
		var currentImage = imageArray[i];
		createImagesView(currentImage);
	}
	imageCaptionView.views = views;
	if(views.length != 0) {
		imageArea.add(imageCaptionView);
	}
}

// creates the views for an image and caption
function createImagesView(currentImage) {
	var imageID = currentImage['image_id'];
	var caption = currentImage['title'];
	//var caption = 'test caption';
	Ti.API.info('image ID: ' + imageID + '\ncaption: ' + caption);
	var url = 'http://www.wtungsten.com/markerimages/' + markerID + '/' + imageID + '.jpg';
	Ti.API.info(url);
	var annoImage = Ti.UI.createImageView({
		top: 10,
		height: '85%',
		image: url
	});
	
	var captionLabel = Ti.UI.createLabel({
		color: 'black',
		text: caption,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
		font: { fontSize: 12 },
		width: '95%',
		height: '15%',
		left: 10,
	});
	
	var holderView = Ti.UI.createView({
		height: Ti.UI.SIZE,
	});
	holderView.layout = 'vertical';
	
	/*annoImage.addEventListener('singletap', function (e){
		Ti.API.info('clicked images');
		var imageWindow = Ti.UI.createWindow({
			fullscreen:false,
			navBarHidden:true,
			backgroundColor: 'black'
		});
		var enlargedImage = Ti.UI.createImageView({
			fullscreen:false,
			navBarHidden:true,
			backgroundColor: 'black',
			image: url
		});	
		imageWindow.add(enlargedImage);
		imageWindow.open();
	});*/
	
	holderView.add(annoImage);
	holderView.add(captionLabel);
	views.push(holderView);
}

$.openAnnotationWindow = function () {
	//heading.setText(location);
	/*descrip.setText('Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description\n
		Test Description' + spacer);*/
	percentage = (1-(50/(deviceHeight/deviceDPI)))*100 + '%';
	annoScrollView.height = percentage;
	//annoImage.setImage('/images/agoraBig.jpg');
	annoWindow.open();
	annoOpen = true;
};

annoWindow.addEventListener('open', function() {
	var actionBar = annoWindow.activity.actionBar;
	actionBar.setDisplayShowHomeEnabled(false);
	actionBar.displayHomeAsUp = true;
	//actionBar.setDisplayShowTitleEnabled(false);
	actionBar.onHomeIconItemSelected = function() 
	{ 
		annoWindow.close();
	};
});

var favBtn = null;

annoWindow.activity.onCreateOptionsMenu = function(e) {
	var menu = e.menu;

	favBtn = menu.add
	({
		title : "Favourite",
		icon : "/images/notfavourite.png",
		showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS
	}); 
	
	if(isFavourite) {
		favBtn.title = 'Favourite';
		favBtn.icon = "/images/favourite.png";
	} else {
		favBtn.title = 'Not\nFavourite';
		favBtn.icon = "/images/notfavourite.png";
	}
	
	favBtn.addEventListener('click', function (e) 
	{
		if(isFavourite) {
			removeFavourite(markerID);
			//$.favourites = Alloy.createController('favourites');
			//$.favourites.updateList();
			favouritesWindow.close();
			Ti.API.info('not favourite now');
			favBtn.title = 'Not\nFavourite';
			favBtn.icon = "/images/notfavourite.png";
		} else {
			addFavourite(markerID, markerName, markerLat, markerLon);
			Ti.API.info('favourite now');
			favBtn.title = 'Favourite';
			favBtn.icon = "/images/favourite.png";
		}
		isFavourite = !isFavourite;
	});
};
 
// routeTo eventlistener button within the annotation window 
routeToButton.addEventListener('click', function(e) {
	destinationName = heading.getText();
	annoWindow.close();
	searchWindow.close();
	categoriesWindow.close();
	favouritesWindow.close();
	annoOpen = false;
	searchOpen = false;
	selectedCategory = false;
	categoriesOpen = false;
	$.routing.newRoute();
});

// routeFrom eventlistener button within the annotation window
routeFromButton.addEventListener('click', function(e) {
	originName = heading.getText();
	annoWindow.close();
	annoOpen = false;
	var toast = Ti.UI.createNotification({
		message: 'Routing from ' + originName,
		duration: Ti.UI.NOTIFICATION_DURATION_LONG
	});
	toast.show();
});


var isFavourite = false;
// checks if the location is a favourite
function checkFavourite(mid) {
	isFavourite = false;
	var favourite = dataBase.execute('SELECT id FROM favourites WHERE id=?', mid);
	while (favourite.isValidRow() && !isFavourite) {
		var id = favourite.fieldByName('id');
		if (id == mid) {
			isFavourite = true;
		}
		favourite.next();
	}
	
	if(isFavourite) {
		Ti.API.info('this is a favourite');
	} else {
		Ti.API.info('this is NOT a favourite');
	}
	favourite.close();
}

// adds to favourites
function addFavourite (mid, name, lat, lon) {
	Ti.API.info('MARKER NAME: ' + name);
	dataBase.execute('INSERT INTO favourites(id, name, lat, lon) VALUES (?,?,?,?)', mid, name, lat, lon);
	//dataBase.close();
};

// removes from favourites
function removeFavourite (mid) {
	dataBase.execute('DELETE FROM favourites WHERE id=?', mid);
	//dataBase.close();
};