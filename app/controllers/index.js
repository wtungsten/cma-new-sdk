//Alloy.Globals.Index = $;

function doClick(e) {
    alert($.label.text);
}

function report(e) {
    Ti.API.info("Annotation " + e.title + " clicked, id: " + e.annotation.myid);
}

//creating controllers
$.categories = Alloy.createController('categories');
$.search = Alloy.createController('search');
$.annotation = Alloy.createController('annotation');
$.menu = Alloy.createController('menu');
$.routing = Alloy.createController('routing');

// retrieves the saved URL call dependant on OS and sends it to be handled
function openURL() {
    Ti.API.info('opening');
    if (Titanium.Platform.osname == 'iphone') {
        
        // Handle the URL in case it opened the app
        handleURL(Ti.App.getArguments().url);
        
        // Handle the URL in case it resumed the app
        Ti.App.addEventListener('resume', function () {
            handleURL(Ti.App.getArguments().url);
        });
        
    } else if (Titanium.Platform.osname == 'android') {
        
        // On Android, somehow the app always opens as new
        handleURL(Alloy.Globals.url);
    }
}
 
// Source: https://github.com/FokkeZB/UTiL/blob/master/XCallbackURL/XCallbackURL.js
// extracts the ID from a URL call and sends it to the annotation controller
$.XCallbackURL = Alloy.createController('XCallbackURL');
function handleURL(url) {
    Ti.API.info('handling');
    var URL = $.XCallbackURL.parse(url),
        controller = URL.action(),
        args = URL.params(),
        id = URL.params('id');
       Ti.API.info('id: ' + JSON.stringify(id)); 
    var mid = id['id'];
    Ti.API.info('MID: ' + mid);
    // Add some better logic here ;)
    if(mid != null) {
    	$.annotation.retrieveLocationDetails(mid);
    	$.annotation.openAnnotationWindow();
    }
}

Ti.UI.setBackgroundColor('#000');

var animation = require('alloy/animation'); 

// Create Action Menu Start for Map Disply window
var searchbtn;

// makes the title bar pressable to open the menu
win.addEventListener('open', function(evt) { 
    var actionBar = win.activity.actionBar; 
    actionBar.setIcon('/images/menu.png');
    actionBar.onHomeIconItemSelected = function() 
    { 
    	 //Hides the about window if user does not close it
		$.menu.closeDialog();
		//hidebuttons();
		//toggleSearch = false;
		$.menu.addMenuWindow();
		$.menu.animateMenu();	
    };
    openURL(); 
});

// creates the buttons in the action bar
win.activity.onCreateOptionsMenu = function(e) { 
	//Creates the menu
	var menu = e.menu; 
	
	clrbtn = menu.add
	({
		title : "Clear",
		icon : "images/clear.png",
		showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS,
		visible : false,
	}); 
	
	searchbtn = menu.add
	({
		title : "Search",
		icon : "images/search.png",
		showAsAction : Ti.Android.SHOW_AS_ACTION_ALWAYS,
	});
	
	clrbtn.addEventListener('click', function (e) 
	{
		$.routing.clearRoute();
		//Ti.API.info('searchboxes: ' + searchboxes);
		clrbtn.setVisible(false);
		Ti.API.info('cleared route');
	});
	
	searchbtn.addEventListener('click', function(e) 
	{
		//Hides the about window if user does not close it
		//menu.setDisplayHomeEnabled(false);
		$.menu.closeDialog();
		
		if ($.menu.getMenuOpen()) {
			//menuWindow.setVisible(false);
			$.menu.animateMenu();
			//hidebuttons();
			Ti.API.info('here2');
		}
		
		$.search.openSearch();
	});
		
};
// Create action menu end for map display window

// Define default region
var region = {
    latitude: -37.7207471,
    longitude: 145.04839,
    latitudeDelta: 0.01,
    longitudeDelta: 0.01
};

// Create map 
map = gmaps.createView({
    enableZoomControls: false,
    userLocationButton: false,
   	traffic: true,
    animate: true,
    height: Ti.UI.FILL,
    mapType: gmaps.NORMAL_TYPE,
    region: region,
    regionFit: true,
    userLocation: true,
    width: Ti.UI.FILL
});
win.add(map);

// Map eventListeners start
map.addEventListener('pinchangedragstate', function(e){
	
	//hides the side menu if opened
    //menuWindow.setVisible(false);
    $.menu.closeMenu();
	sliding = false; 
    toggle = false;
    win.left = 0;
	
    Ti.API.info(e.type);
    Ti.API.info(JSON.stringify(e.newState));
}); 

//Checks which section as well as which annotation is 
//selected and notifies us in the console window
map.addEventListener('click', function(e){
	
	//hides the side menu if opened
	//menuWindow.setVisible(false);
	$.menu.closeMenu();
	sliding = false; 
	toggle = false;
	win.left = 0;
	
    Ti.API.info(e.type);
    Ti.API.info("Annotation ID: " + e.annotation.myid);
    Ti.API.info("Annotation Title: " + e.annotation.title);
    Ti.API.info(JSON.stringify(e.clicksource));
});

map.addEventListener('click', function(e){
	// retrieves annotations title and saves it to selectedLocation
	$.annotation.retrieveLocationDetails(e.annotation.mid);
    if (e.clicksource == 'title' || e.clicksource == 'infoWindow') {
    	Ti.API.info("selected annotation title");
    	selectedLocation = e.annotation.title;
  		$.annotation.openAnnotationWindow();
  		//Ti.API.info('Percentage: ' + percentage);
 	};
});


//Updates With coordinates that are on the center of the screen
map.addEventListener('regionchanged', function(e){
	
	//hides the side menu and about window if opened
	$.menu.closeDialog();
	//menuWindow.setVisible(false);
	//toggle = false;
	//toggleSearch = false;
	if($.menu.getMenuOpen())
	{
		$.menu.animateMenu();
	}
	
	/*if(e.latitudeDelta > 0.01 && e.longitudeDelta > 0.01)
	{
		Ti.API.info(e.latitudeDelta + " ,,,, " + e.longitudeDelta);
		map.region = ({latitude: e.latitude, longitude: e.longitude, 
				latitudeDelta: 0.01, longitudeDelta: 0.01});
	}*/
    Ti.API.info(e.type);
    Ti.API.info(e.latitude + "," + e.longitude);
    //hidebuttons();
});

map.addEventListener('complete', function(e){
    Ti.API.info(e.type);
});
// Map eventListeners end

// Window eventListeners start
win.addEventListener('android:back',function(e) {
	if ($.menu.getMenuOpen())
	{
		$.menu.animateMenu();	
	}
	else
	{
		win.close();
    	var activity = Titanium.Android.currentActivity;
    	dataBase.close();
    	activity.finish();
	}
});
// Window eventListeners end

// checks for rotation
Ti.Gesture.addEventListener('orientationchange', function(e){
	Ti.API.info('rotated');
	$.menu.removeWindow();
});

// compare function to sort lists
function compare(a,b) {
  if (a.title.toLowerCase() < b.title.toLowerCase())
     return -1;
  if (a.title.toLowerCase() > b.title.toLowerCase())
    return 1;
  return 0;
}

// converts a list to listViewRows
var listToRow = [];
function listToRows(list) {
	listToRow = [];
	for(var d in list) {
		var name = list[d]['title'];
	
		var row = Ti.UI.createTableViewRow({
			height: 50,
			className: "tableRow"
		});
		
		var label = Ti.UI.createLabel({
			text: name,
			color: 'black',
			font: { fontsize: 16 },
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
			left: 10,
			height: 50
		});
		
		Ti.API.info("row add :" + name);
		row.add(label);
		listToRow.push(row);
	}
}

win.open();
//dataBaseBuild();
retrieveAllLocations();
//win.add(menuWindow);
