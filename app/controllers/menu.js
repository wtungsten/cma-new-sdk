$.categories = Alloy.createController('categories'); // connection to categories controller
$.favourites = Alloy.createController('favourites'); // connection to favourites controller

var menuOpen = false;
// animations for the menu to slide in and out 
var slideRight = Ti.UI.createAnimation({left: '0%', duration: 150});
var slideLeft = Ti.UI.createAnimation({left: '-50%', duration: 150});

// opens the menu window
$.addMenuWindow = function () {
	win.add(menuWindow);
	Ti.API.info('added menu');
	menuItems();
};

$.removeWindow = function () {
	if(!menuOpen){
		win.remove(menuWindow);
	}
};

// explicitly close menu 
$.closeMenu = function () {
	menuWindow.animate(slideLeft);
	menuOpen = false;
};

// forward to animateMenu function
$.animateMenu = function () {
	animateMenu();
};

// returns if the menu is open
$.getMenuOpen = function () {
	return menuOpen;
};

// closes the dialog box
$.closeDialog = function () {
	customDialog.hide();
};

// this method will open or close the menu
function animateMenu ()
{
	if (menuOpen)
	{
        Ti.API.info('closed menu');
        //menuWindow.setVisible(true);
        toggleMenu = true;
        //menuWindow.left = '-50%'; //43.75% original
        //slideLeft.left = 140*deviceDPI; //43.75% original
        //slideLeft.duration = 150;
        menuWindow.animate(slideLeft); 
        menuOpen = false;
    }
    else
    {
    	//win.add(menuWindow);
    	Ti.API.info('open menu');   	
        //menuWindow.setVisible(true);
        toggleMenu = true;
        //menuWindow.left = '0%';
        //slideRight.left = 140*deviceDPI;
        //slideRight.duration = 150;
        menuWindow.animate(slideRight);
        menuOpen = true;
    }
}

// Custom about window start
var customDialog = Ti.UI.createView({
	height : 400,
	width : 300,
	backgroundColor : "#2D251A",
	borderRadius : 5
})

var titleLabel = Ti.UI.createLabel({
    top : 10,
    text : "Campus Maps Applicaton (CMA) v?.?? \n 
    Changes \n 
    - Fixed bug with search button
    - Fixed several bugs with UI elements not 
      appearing on a single click \n
    - Fixed overlapping of certain UI elements 
      now only one element is displyed at a time 
    - Fixed notification bar dissapearing when 
      app is loading then reappearing when the
      map is displayed \n
    - Annotation page is now themed
    - Action bar is now themed
    - UI elements are now themed
    - Options in menu have icons
    - La Trobe logos have replaced Tungsten ones \n
    - Revamped the about page to conform with UI
    - Menu button is now the app logo
    - Search and clear buttons are now integrated
      within action bar",
    color : "#FFF",
    font : {
 		fontSize : 12,
        fontWeight : "bold"
    }
})
customDialog.add(titleLabel);
 
var cancelBtn = Ti.UI.createButton({
    bottom : 5,
    left : 10,
    width : 280,
    height : 40,
    title : "Close",
    color : "#FFF"
});
customDialog.add(cancelBtn);
// Custom about window end

menuWindow = Ti.UI.createView({
    top : 0,
    left : '-50%',
    width : '50%',
    backgroundColor:'#63513D',
    //visible: false,
    visible: true
});
//win.add(menuWindow);

//// ---- Menu Table
// Menu Titles
/*var menuTitles = [
	{title: 'Categories', leftImage: 'images/categories.png'},
	{title: 'Favourites', leftImage: 'images/favorites.png'},
	{title: 'Retrieve Locations', leftImage: 'images/retrieve.png'},
	{title: 'About', leftImage: 'images/about.png'},
];*/

// creates the list of menu items
// to add to the list, add the item name in menuList, and then add the image for it in menuImages
var menuList = ['Categories', 'Favourites', 'Update DB', 'About'];
var menuImages = ['categories', 'favorites', 'retrieve', 'about'];
var menuTitles = [];

// main view for the window
var tableView = Ti.UI.createTableView({
		data : menuTitles,
    	minRowHeight: 50
});

// creates the options labels in the menu
function menuItems() {
	menuTitles = [];
	for(var m in menuList) {
		var text = menuList[m];
		var image = menuImages[m];
		
		var rowView = Ti.UI.createTableViewRow({
			height: 50,
			className: 'menuList'
		});
		
		var menuLabel = Ti.UI.createLabel({
			text: text,
			color: 'white',
			font: { fontsize: 6 },
			textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
			left: 10,
			//borderColor: 'red'
		});
		
		var menuImage = Ti.UI.createImageView({
			width: Ti.UI.SIZE,
			image: '/images/' + image + '.png',
			//borderColor: 'green'
		});
		
		var menuView = Ti.UI.createView({
			width: Ti.UI.FILL,
			layout: 'horizontal',
			//borderColor: 'blue'
		});
		
		menuView.add(menuImage);
		menuView.add(menuLabel);
		
		rowView.add(menuView);
		menuTitles.push(rowView);
		Ti.API.info('MENU: ' + text + ' & ' + image);
	}
	tableView.data = menuTitles;	
}

//tableView.selectionIndicator=true;

menuWindow.add(tableView);
//var toggle = false;
var toggleMenu = false;
var toggleSearch = false;
//Redundant Action Menu bar supersedes this
/*menubtn.addEventListener('click', function(e) {
    if (toggle) {
        menuWindow.setVisible(false);
        toggle = false;
        win.left = 0;
        //menubtn.left = 11;
    } else {
    	//win.add(menuWindow);
        menuWindow.setVisible(true);
        toggle = true;
        win.left = 100;
        //menubtn.left = 110;
    }
});*/

// determines what option was selected
tableView.addEventListener('click', function(e)
{
	var select = e.index;
	
	if (e.index == 0)
	{
		Ti.API.info('Categories');
		//alert('Selected categories');
		//retrieveCategories();
		//categoriesOpen = true;
		$.categories.openCategories();
	}
	
	else if (e.index == 1)
	{
		Ti.API.info('Favourites');
		//alert('Selected favorities');
		$.favourites.openFavourites();
	}
	
	else if (e.index == 2)
	{
		//map.removeAllAnnotations();
		retrieveAllLocations();
	}
	
	else if (e.index == 3)
	{
		Ti.API.info('About');
		
		//Hides all other elements to view the about information
		//menuWindow.setVisible(false);
		sliding = false; 
		toggleMenu = false;
		//toggleSearch = false;
		
		//Show the custom view
		win.add(customDialog);
		customDialog.show();
		 
		cancelBtn.addEventListener('click',function(){
		    customDialog.hide()
		});
	
		animateMenu();
	}
	
	else
	{
		Ti.API.info('No option selected');
		alert('No option selected');
	}
});
