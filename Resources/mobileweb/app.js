var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.Map = require("ti.map");

var MapModule = require("ti.map");

var win = Ti.UI.createWindow({
    fullscreen: false,
    navBarHidden: true,
    backgroundColor: "black"
});

var map1 = MapModule.createView({
    userLocation: true,
    mapType: MapModule.NORMAL_TYPE,
    animate: true,
    region: {
        latitude: -37.7208312,
        longitude: 145.0485824,
        latitudeDelta: .01,
        longitudeDelta: .01
    }
});

win.add(map1);

win.open();

var rc = MapModule.isGooglePlayServicesAvailable();

switch (rc) {
  case MapModule.SUCCESS:
    Ti.API.info("Google Play services is installed.");
    break;

  case MapModule.SERVICE_MISSING:
    alert("Google Play services is missing. Please install Google Play services from the Google Play store.");
    break;

  case MapModule.SERVICE_VERSION_UPDATE_REQUIRED:
    alert("Google Play services is out of date. Please update Google Play services.");
    break;

  case MapModule.SERVICE_DISABLED:
    alert("Google Play services is disabled. Please enable Google Play services.");
    break;

  case MapModule.SERVICE_INVALID:
    alert("Google Play services cannot be authenticated. Reinstall Google Play services.");
    break;

  default:
    alert("Unknown error.");
}

Alloy.createController("index");