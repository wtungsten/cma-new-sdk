function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.label1 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#FFF",
        id: "label1",
        text: "MapsTester v1.0",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        top: "100"
    });
    $.__views.index.add($.__views.label1);
    $.__views.label2 = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#FFF",
        id: "label2",
        text: "Please run on a device, emulators are not supported",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        top: "150"
    });
    $.__views.index.add($.__views.label2);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var agora = MapModule.createAnnotation({
        latitude: -37.7207471,
        longitude: 145.04839,
        pincolor: MapModule.ANNOTATION_RED,
        myid: 1,
        rightButton: Ti.UI.createButton({
            title: "More Info"
        }),
        leftButton: "/images/agoraTiny.jpg",
        title: "The Agora",
        subtitle: "La Trobe University"
    });
    var bethGleeson = MapModule.createAnnotation({
        latitude: -37.72142379451585,
        longitude: 145.04653338342905,
        pincolor: MapModule.ANNOTATION_RED,
        myid: 2,
        rightButton: Ti.UI.createButton({
            title: "More Info"
        }),
        leftButton: "",
        title: "Beth Gleeson",
        subtitle: "La Trobe University"
    });
    map1.addEventListener("pinchangedragstate", function(e) {
        Ti.API.info(e.type);
        Ti.API.info(JSON.stringify(e.newState));
    });
    map1.addEventListener("click", function(e) {
        Ti.API.info(e.type);
        Ti.API.info("Annotation ID: " + e.annotation.myid);
        Ti.API.info(JSON.stringify(e.clicksource));
    });
    map1.addEventListener("regionchanged", function(e) {
        Ti.API.info(e.type);
        Ti.API.info(e.latitude + "," + e.longitude);
    });
    map1.addEventListener("complete", function(e) {
        Ti.API.info(e.type);
    });
    1 == e.annotation.myid && map1.addEventListener("click", function(e) {
        "rightPane" == e.clicksource && Ti.UI.createAlertDialog({
            message: "The Agora Is the social area of La Trobe where people come to converse as well as grab a bite",
            ok: "Okay",
            title: "About"
        }).show();
        if ("leftPane" == e.clicksource) {
            var win = Ti.UI.createWindow({
                fullscreen: false,
                navBarHidden: true
            });
            var image = Ti.UI.createImageView({
                image: "/images/agoraBig.jpg"
            });
            win.add(image);
            win.open();
        }
    });
    map1.addAnnotation(agora);
    map1.addAnnotation(bethGleeson);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;