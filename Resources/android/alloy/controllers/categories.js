function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function retrieveCategories() {
        var xhr = Ti.Network.createHTTPClient();
        xhr.onload = function() {
            var response = JSON.parse(this.responseText);
            Ti.API.info(response.result);
            var error = JSON.stringify(response.error);
            var msg = JSON.stringify(response.msg);
            Ti.API.info(error);
            if ("true" == error) {
                categoriesListItems = [];
                categoriesListItems.push({
                    title: msg,
                    error: true
                });
            } else {
                serverCategoriesList = response.result;
                categoriesListItems = [];
                for (var d in serverCategoriesList) {
                    var cid = serverCategoriesList[d]["id"];
                    var cname = serverCategoriesList[d]["name"];
                    categoriesListItems.push({
                        cid: cid,
                        title: cname
                    });
                }
                Ti.API.info("After loop");
            }
            addCategoriesListItems();
        };
        xhr.onerror = function(e) {
            alert("error: ", JSON.stringify(e));
        };
        xhr.open("POST", "http://www.wtungsten.com/app/functions.php");
        xhr.send({
            action: "categories"
        });
    }
    function addCategoriesListItems() {
        Ti.API.info("adding lists");
        categoriesView.removeAllChildren();
        categoriesList.data = [];
        categoriesListItems.sort(compare);
        listToRows(categoriesListItems);
        categoriesList = Ti.UI.createTableView({
            minRowHeight: 50,
            data: listToRow,
            separatorColor: "#FF9E1B"
        });
        categoriesView.add(categoriesList);
        categoriesList.addEventListener("click", function(e) {
            if (!categoriesListItems[e.index]["error"]) {
                var category = categoriesListItems[e.index]["cid"];
                selectedCategory = true;
                retrieveLocationsOfCategory(category);
                categoriesWindow.title = categoriesListItems[e.index]["title"];
            }
        });
    }
    function retrieveLocationsOfCategory(category) {
        var xhr = Ti.Network.createHTTPClient();
        xhr.onload = function() {
            var response = JSON.parse(this.responseText);
            Ti.API.info(response.result);
            var error = JSON.stringify(response.error);
            var msg = JSON.stringify(response.msg);
            Ti.API.info(error);
            if ("true" == error) {
                categoryLocations = [];
                categoryLocations.push({
                    title: msg,
                    error: true
                });
            } else {
                serverCategoryLocations = response.result;
                categoryLocations = [];
                for (var d in serverCategoryLocations) {
                    var mid = serverCategoryLocations[d]["marker_id"];
                    var mname = serverCategoryLocations[d]["marker_name"];
                    var lat = serverCategoryLocations[d]["latitude"];
                    var lon = serverCategoryLocations[d]["longitude"];
                    categoryLocations.push({
                        mid: mid,
                        title: mname,
                        lat: lat,
                        lon: lon
                    });
                    Ti.API.info("In loop");
                }
                Ti.API.info("After loop");
            }
            addMarkersOfCategoryItems();
        };
        xhr.onerror = function(e) {
            alert("error: ", JSON.stringify(e));
        };
        xhr.open("POST", "http://www.wtungsten.com/app/functions.php");
        xhr.send({
            action: "markersOfType",
            search: category
        });
    }
    function addMarkersOfCategoryItems() {
        Ti.API.info("adding lists");
        categoriesView.removeAllChildren();
        categoriesList.data = [];
        categoryLocations.sort(compare);
        listToRows(categoryLocations);
        categoriesList = Ti.UI.createTableView({
            minRowHeight: 50,
            data: listToRow,
            separatorColor: "#FF9E1B"
        });
        categoriesView.add(categoriesList);
        categoriesList.addEventListener("click", function(e) {
            if (!categoryLocations[e.index]["error"]) {
                var mid = categoryLocations[e.index]["mid"];
                var lat = categoryLocations[e.index]["lat"];
                var lon = categoryLocations[e.index]["lon"];
                Ti.API.info("Marker ID: " + mid);
                map.region = {
                    latitude: lat,
                    longitude: lon,
                    latitudeDelta: .0075,
                    longitudeDelta: .0075
                };
                $.annotation.retrieveLocationDetails(mid);
                $.annotation.openAnnotationWindow();
            }
        });
    }
    function closeCategories() {
        if (selectedCategory) {
            addCategoriesListItems();
            selectedCategory = false;
            Ti.API.info("selected category backpressed 2");
            categoriesWindow.title = "Categories";
        } else {
            categoriesWindow.close();
            categoriesOpen = false;
            Ti.API.info("category backpressed 2");
        }
    }
    function compare(a, b) {
        if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
        if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
        return 0;
    }
    function listToRows(list) {
        listToRow = [];
        for (var d in list) {
            var name = list[d]["title"];
            var row = Ti.UI.createTableViewRow({
                height: 50,
                className: "tableRow"
            });
            var label = Ti.UI.createLabel({
                text: name,
                color: "black",
                font: {
                    fontsize: 16
                },
                textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
                left: 10,
                height: 50
            });
            Ti.API.info("row add :" + name);
            row.add(label);
            listToRow.push(row);
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "categories";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.categories = Ti.UI.createView({
        id: "categories"
    });
    $.__views.categories && $.addTopLevelView($.__views.categories);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.openCategories = function() {
        categoriesWindow.open();
        retrieveCategories();
    };
    $.annotation = Alloy.createController("annotation");
    categoriesWindow = Ti.UI.createWindow({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white",
        title: "Categories"
    });
    categoriesWindow.layout = "vertical";
    var categoriesView = Ti.UI.createScrollView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "#white"
    });
    categoriesWindow.add(categoriesView);
    var categoriesOpen = false;
    var serverCategoriesList = [];
    var categoriesListItems = [];
    var categoriesList = Ti.UI.createTableView({});
    var serverCategoryLocations = [];
    var categoryLocations = [];
    var selectedCategory = false;
    categoriesWindow.addEventListener("android:back", function() {
        closeCategories();
    });
    categoriesWindow.addEventListener("open", function() {
        var actionBar = categoriesWindow.activity.actionBar;
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.displayHomeAsUp = true;
        actionBar.onHomeIconItemSelected = function() {
            closeCategories();
        };
    });
    var listToRow = [];
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;