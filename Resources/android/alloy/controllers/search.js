function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function searchLocations(search) {
        var xhr = Ti.Network.createHTTPClient();
        xhr.onload = function() {
            var response = JSON.parse(this.responseText);
            Ti.API.info(response.result);
            var error = JSON.stringify(response.error);
            var msg = JSON.stringify(response.msg);
            msg = msg.replace(/"/g, "");
            Ti.API.info(error);
            if ("true" == error) {
                searchListItems = [];
                "No results" == msg && searchListItems.push({
                    title: msg,
                    error: true
                });
            } else {
                serverSearchList = response.result;
                searchListItems = [];
                for (var d in serverSearchList) {
                    Ti.API.info("In loop");
                    var mid = serverSearchList[d]["marker_id"];
                    var name = serverSearchList[d]["marker_name"];
                    var tid = serverSearchList[d]["type_id"];
                    var tname = serverSearchList[d]["type_name"];
                    var lat = serverSearchList[d]["latitude"];
                    var lon = serverSearchList[d]["longitude"];
                    Ti.API.info(name);
                    searchListItems.push({
                        mid: mid,
                        title: name,
                        tid: tid,
                        tname: tname,
                        lat: lat,
                        lon: lon
                    });
                }
                Ti.API.info("After loop");
            }
            addSearchListItems();
        };
        xhr.onerror = function(e) {
            alert("error: ", JSON.stringify(e));
        };
        xhr.open("POST", "http://www.wtungsten.com/app/functions.php");
        xhr.send({
            action: "searchMarkers",
            search: search
        });
    }
    function addSearchListItems() {
        Ti.API.info("adding lists");
        searchScrollView.removeAllChildren();
        searchList.data = [];
        searchListItems.sort(compare);
        listToRows(searchListItems);
        Ti.API.info("listToRow: " + JSON.stringify(listToRow));
        searchList = Ti.UI.createTableView({
            minRowHeight: 50,
            data: listToRow,
            separatorColor: "#FF9E1B"
        });
        Ti.API.info("listToRow: " + JSON.stringify(searchList.data));
        searchScrollView.add(searchList);
        searchList.addEventListener("click", function(e) {
            if (!searchListItems[e.index]["error"]) {
                var mid = searchListItems[e.index]["mid"];
                var item = searchListItems[e.index]["title"];
                searchListItems[e.index]["tid"];
                searchListItems[e.index]["tname"];
                var lat = searchListItems[e.index]["lat"];
                var lon = searchListItems[e.index]["lon"];
                Ti.API.info(item);
                map.region = {
                    latitude: lat,
                    longitude: lon,
                    latitudeDelta: .0075,
                    longitudeDelta: .0075
                };
                $.annotation.retrieveLocationDetails(mid);
                $.annotation.openAnnotationWindow();
            }
        });
    }
    function compare(a, b) {
        if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
        if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
        return 0;
    }
    function listToRows(list) {
        listToRow = [];
        for (var d in list) {
            var name = list[d]["title"];
            var row = Ti.UI.createTableViewRow({
                height: 50,
                className: "tableRow"
            });
            var label = Ti.UI.createLabel({
                text: name,
                color: "black",
                font: {
                    fontsize: 16
                },
                textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
                left: 10,
                height: 50
            });
            Ti.API.info("row add :" + name);
            row.add(label);
            listToRow.push(row);
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "search";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.search = Ti.UI.createView({
        id: "search"
    });
    $.__views.search && $.addTopLevelView($.__views.search);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.openSearch = function() {
        searchWindow.open();
    };
    $.annotation = Alloy.createController("annotation");
    searchWindow = Ti.UI.createWindow({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white",
        title: "Search Locations"
    });
    searchWindow.layout = "vertical";
    Ti.UI.createView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white"
    });
    Ti.UI.createButton({
        height: 39,
        width: 39,
        right: 0,
        borderWidth: 1,
        opacity: .7,
        backgroundTopCap: 10,
        backgroundImage: "/images/search.png",
        backgroundColor: "#FFF",
        color: "#666",
        borderColor: "#666"
    });
    var searchBar = Titanium.UI.createTextField({
        barColor: "#000",
        color: "black",
        showCancel: false,
        height: 50,
        width: Titanium.UI.FILL,
        top: 0,
        opacity: 1,
        hintText: "Search",
        returnKeyType: Ti.UI.RETURNKEY_SEARCH
    });
    var searchScrollView = Ti.UI.createScrollView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white"
    });
    searchWindow.add(searchBar);
    searchWindow.add(searchScrollView);
    searchBar.addEventListener("return", function() {
        searchLocations(searchBar.value);
    });
    var serverSearchList = [];
    var searchListItems = [];
    var searchList = Ti.UI.createTableView({});
    searchWindow.addEventListener("open", function() {
        var actionBar = searchWindow.activity.actionBar;
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.displayHomeAsUp = true;
        actionBar.onHomeIconItemSelected = function() {
            searchWindow.close();
        };
    });
    var listToRow = [];
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;