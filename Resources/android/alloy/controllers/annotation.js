function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function addDetails(details) {
        var title = JSON.stringify(details["marker_name"]);
        title = title.replace(/"/g, "");
        Ti.API.info("title: " + title);
        annoWindow.setTitle(title);
        var description = JSON.stringify(details["description"]);
        Ti.API.info(description);
        description = description.replace(/"/g, "");
        description = description.replace(/\\r/g, "");
        Ti.API.info(description);
        description = description.replace(/\\n/g, "\n");
        Ti.API.info(description);
        var inputs = JSON.stringify(details["inputs"]);
        Ti.API.info(inputs);
        inputs = inputs.replace(/[{}\[\]\\\"]/g, "");
        inputs = inputs.replace(/:/g, ": ");
        inputs = inputs.replace(/,/g, "\n");
        Ti.API.info(inputs);
        description = description + "\n\n" + inputs;
        heading.setText(title);
        descrip.setText(description);
        var imageArray = details["images"];
        imageArea.removeAllChildren();
        views = [];
        for (var i in imageArray) {
            var currentImage = imageArray[i];
            createImagesView(currentImage);
        }
        imageCaptionView.views = views;
        0 != views.length && imageArea.add(imageCaptionView);
    }
    function createImagesView(currentImage) {
        var imageID = currentImage["image_id"];
        var caption = currentImage["title"];
        Ti.API.info("image ID: " + imageID + "\ncaption: " + caption);
        var url = "http://www.wtungsten.com/markerimages/" + markerID + "/" + imageID + ".jpg";
        Ti.API.info(url);
        var annoImage = Ti.UI.createImageView({
            top: 10,
            height: "85%",
            image: url
        });
        var captionLabel = Ti.UI.createLabel({
            color: "black",
            text: caption,
            textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
            verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
            font: {
                fontSize: 12
            },
            width: "95%",
            height: "15%",
            left: 10
        });
        var holderView = Ti.UI.createView({
            height: Ti.UI.SIZE
        });
        holderView.layout = "vertical";
        holderView.add(annoImage);
        holderView.add(captionLabel);
        views.push(holderView);
    }
    function checkFavourite(mid) {
        isFavourite = false;
        var favourite = dataBase.execute("SELECT id FROM favourites WHERE id=?", mid);
        while (favourite.isValidRow() && !isFavourite) {
            var id = favourite.fieldByName("id");
            id == mid && (isFavourite = true);
            favourite.next();
        }
        isFavourite ? Ti.API.info("this is a favourite") : Ti.API.info("this is NOT a favourite");
        favourite.close();
    }
    function addFavourite(mid, name, lat, lon) {
        Ti.API.info("MARKER NAME: " + name);
        dataBase.execute("INSERT INTO favourites(id, name, lat, lon) VALUES (?,?,?,?)", mid, name, lat, lon);
    }
    function removeFavourite(mid) {
        dataBase.execute("DELETE FROM favourites WHERE id=?", mid);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "annotation";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.annotation = Ti.UI.createView({
        id: "annotation"
    });
    $.__views.annotation && $.addTopLevelView($.__views.annotation);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.routing = Alloy.createController("routing");
    var annoWindow = Ti.UI.createWindow({
        fullscreen: false,
        navBarHidden: false,
        backgroundColor: "white",
        title: "Loading..."
    });
    var annoOpen = false;
    var percentage = 100 * (1 - 50 / (deviceHeight / deviceDPI)) + "%";
    var annoView = Ti.UI.createView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white"
    });
    annoView.layout = "vertical";
    var annoScrollView = Ti.UI.createScrollView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white",
        height: percentage,
        top: 0
    });
    var imageArea = Ti.UI.createView({
        height: Ti.UI.SIZE
    });
    var imageCaptionView = Ti.UI.createScrollableView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "#EBE8E1",
        height: "50%",
        top: 0
    });
    var views = [];
    imageCaptionView.layout = "vertical";
    annoView.add(imageArea);
    var heading = Ti.UI.createLabel({
        color: "black",
        text: "Test",
        font: {
            fontSize: 24
        },
        width: "95%",
        left: 10
    });
    var descrip = Ti.UI.createLabel({
        color: "black",
        text: "TLoading...",
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP,
        font: {
            fontSize: 16
        },
        width: "95%",
        left: 10
    });
    annoView.add(descrip);
    var buttonView = Ti.UI.createView({
        fullscreen: false,
        navBarHidden: true,
        backgroundColor: "white",
        bottom: 0,
        height: Ti.UI.SIZE
    });
    var routeToButton = Ti.UI.createButton({
        title: "Route To",
        right: 0,
        bottom: 0,
        height: 50,
        width: "50%"
    });
    var routeFromButton = Ti.UI.createButton({
        title: "Route From",
        left: 0,
        bottom: 0,
        height: 50,
        width: "50%"
    });
    buttonView.add(routeToButton);
    buttonView.add(routeFromButton);
    annoScrollView.add(annoView);
    annoWindow.add(annoScrollView);
    annoWindow.add(buttonView);
    var details = [];
    var markerID;
    var markerName;
    var markerLat;
    var markerLon;
    $.retrieveLocationDetails = function(mid) {
        checkFavourite(mid);
        annoWindow.setTitle("Loading...");
        descrip.setText("Loading...");
        var xhr = Ti.Network.createHTTPClient();
        Ti.API.info("retrieving");
        xhr.onload = function() {
            var response = JSON.parse(this.responseText);
            var error = JSON.stringify(response.error);
            JSON.stringify(response.msg);
            markerID = mid;
            if ("true" == error) addDetails({
                marker_name: "Error",
                description: "Location could not be found on the server",
                inputs: ""
            }); else {
                details = response.result;
                markerName = details[0]["marker_name"];
                markerLat = details[0]["latitude"];
                markerLon = details[0]["longitude"];
                addDetails(details[0]);
            }
        };
        xhr.onerror = function(e) {
            alert("error: ", JSON.stringify(e));
        };
        xhr.open("POST", "http://www.wtungsten.com/app/functions.php");
        xhr.send({
            action: "markerFromId",
            search: mid
        });
    };
    $.openAnnotationWindow = function() {
        percentage = 100 * (1 - 50 / (deviceHeight / deviceDPI)) + "%";
        annoScrollView.height = percentage;
        annoWindow.open();
        annoOpen = true;
    };
    annoWindow.addEventListener("open", function() {
        var actionBar = annoWindow.activity.actionBar;
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.displayHomeAsUp = true;
        actionBar.onHomeIconItemSelected = function() {
            annoWindow.close();
        };
    });
    var favBtn = null;
    annoWindow.activity.onCreateOptionsMenu = function(e) {
        var menu = e.menu;
        favBtn = menu.add({
            title: "Favourite",
            icon: "/images/notfavourite.png",
            showAsAction: Ti.Android.SHOW_AS_ACTION_ALWAYS
        });
        if (isFavourite) {
            favBtn.title = "Favourite";
            favBtn.icon = "/images/favourite.png";
        } else {
            favBtn.title = "Not\nFavourite";
            favBtn.icon = "/images/notfavourite.png";
        }
        favBtn.addEventListener("click", function() {
            if (isFavourite) {
                removeFavourite(markerID);
                favouritesWindow.close();
                Ti.API.info("not favourite now");
                favBtn.title = "Not\nFavourite";
                favBtn.icon = "/images/notfavourite.png";
            } else {
                addFavourite(markerID, markerName, markerLat, markerLon);
                Ti.API.info("favourite now");
                favBtn.title = "Favourite";
                favBtn.icon = "/images/favourite.png";
            }
            isFavourite = !isFavourite;
        });
    };
    routeToButton.addEventListener("click", function() {
        destinationName = heading.getText();
        annoWindow.close();
        searchWindow.close();
        categoriesWindow.close();
        favouritesWindow.close();
        annoOpen = false;
        searchOpen = false;
        selectedCategory = false;
        categoriesOpen = false;
        $.routing.newRoute();
    });
    routeFromButton.addEventListener("click", function() {
        originName = heading.getText();
        annoWindow.close();
        annoOpen = false;
        var toast = Ti.UI.createNotification({
            message: "Routing from " + originName,
            duration: Ti.UI.NOTIFICATION_DURATION_LONG
        });
        toast.show();
    });
    var isFavourite = false;
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;