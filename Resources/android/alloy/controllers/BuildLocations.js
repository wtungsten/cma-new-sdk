function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    function locationBuild(mid, mname, tid, tname, lat, long) {
        Ti.API.info("Build");
        var annotation = gmaps.createAnnotation({
            latitude: lat,
            longitude: long,
            image: "/images/pin.png",
            title: mname,
            mid: mid,
            tid: tid,
            tname: tname
        });
        map.addAnnotation(annotation);
        Ti.API.info("added");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "BuildLocations";
    if (arguments[0]) {
        __processArg(arguments[0], "__parentSymbol");
        __processArg(arguments[0], "$model");
        __processArg(arguments[0], "__itemTemplate");
    }
    var $ = this;
    var exports = {};
    $.__views.BuildLocations = Ti.UI.createView({
        id: "BuildLocations"
    });
    $.__views.BuildLocations && $.addTopLevelView($.__views.BuildLocations);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var serverLocations = [];
    $.retrieveAllLocations = function() {
        var xhr = Ti.Network.createHTTPClient();
        xhr.onload = function() {
            var response = JSON.parse(this.responseText);
            Ti.API.info(response.result);
            var error = JSON.stringify(response.error);
            JSON.stringify(response.msg);
            Ti.API.info(error);
            if ("true" == error) ; else {
                locationArray = [];
                serverLocations = response.result;
                for (var d in serverLocations) {
                    var mid = serverLocations[d]["marker_id"];
                    var mname = serverLocations[d]["marker_name"];
                    var tid = serverLocations[d]["type_id"];
                    var tname = serverLocations[d]["type_name"];
                    var lat = serverLocations[d]["latitude"];
                    var long = serverLocations[d]["longitude"];
                    locationArray.push({
                        marker_id: mid,
                        name: mname,
                        type_id: tid,
                        type_name: tname,
                        lat: lat,
                        lon: long
                    });
                    locationBuild(mid, mname, tid, tname, lat, long);
                    Ti.API.info("In loop");
                }
                Ti.API.info("After loop");
            }
        };
        xhr.onerror = function(e) {
            alert("error: ", JSON.stringify(e));
        };
        xhr.open("POST", "http://www.wtungsten.com/app/functions.php");
        xhr.send({
            action: "parentMarkers"
        });
    };
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;